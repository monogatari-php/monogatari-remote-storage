<?php

namespace Monogatari\RemoteStorage\StorageEngine;

use Monogatari\RemoteStorage\Http\StorageRequest;

class SimpleStorageFactory implements StorageFactoryInterface
{

    /**
     * The actual function to call when makeing
     * the StorageInterface with. The function
     * must be with this signature:
     *
     * <code>
     * function (\Monogatari\RemoteStorage\Http\StorageRequest $request):
     *   \Monogatari\RemoteStorage\StorageEngine\StorageInterface;
     * </code>
     *
     * @var callable
     */
    private $callback;

    public function __construct(callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * {@inheritDoc}
     */
    public function makeStorage(StorageRequest $request): StorageInterface
    {
        // call the callable stored in the callback attribute
        return ($this->callback)($request);
    }

}