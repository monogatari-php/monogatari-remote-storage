<?php

namespace Monogatari\RemoteStorage\StorageEngine;

use Monogatari\RemoteStorage\Http\StorageRequest;

/**
 * Factory to create StorageInterface for a certain StorageRequest.
 */
interface StorageFactoryInterface
{
    /**
     * Create a storage for the specifid request.
     *
     * @param StorageRequest $request  The request that needs the Storage.
     *
     * @return StorageInterface  The Storage for the request.
     *                           Could be specific to the store name.
     */
    public function makeStorage(StorageRequest $request): StorageInterface;
}