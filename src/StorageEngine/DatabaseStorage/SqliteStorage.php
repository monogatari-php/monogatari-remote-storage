<?php

namespace Monogatari\RemoteStorage\StorageEngine\DatabaseStorage;

class SqliteStorage extends PDOStorage
{
    /**
     * {@inheritDoc}
     */
    protected function init(): bool
    {
        $stmt = $this->getPDO()->prepare(static::useTablename('
            CREATE TABLE IF NOT EXISTS `{tableName}` (
                `user_id` TEXT NOT NULL DEFAULT "",
                `store_name` TEXT NOT NULL DEFAULT "",
                `key` TEXT NOT NULL DEFAULT "",
                `value` BLOB,
                PRIMARY KEY (`user_id`, `store_name`, `key`)
            );
        ', $this->getTableName()));
        return $stmt->execute();
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareGetAllStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            SELECT `key`, `value` FROM `{tableName}` WHERE
                `user_id` = :user_id AND
                `store_name` = :store_name;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareGetStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            SELECT `value` FROM `{tableName}` WHERE
                `user_id` = :user_id AND
                `store_name` = :store_name AND
                `key` = :key;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareSetStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            REPLACE INTO `{tableName}` (`user_id`, `store_name`, `key`, `value`)
                VALUES (:user_id, :store_name, :key, :value);
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareRemoveStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            DELETE FROM `{tableName}` WHERE
                `user_id` = :user_id AND
                `store_name` = :store_name AND
                `key` = :key;
        ', $this->getTableName()));
    }

    /**
     * {@inheritDoc}
     */
    protected function prepareClearStatement(): \PDOStatement
    {
        return $this->getPDO()->prepare(static::useTablename('
            DELETE FROM `{tableName}` WHERE
                `user_id` = :user_id AND
                `store_name` = :store_name;
        ', $this->getTableName()));
    }
}
