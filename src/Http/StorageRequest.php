<?php

namespace Monogatari\RemoteStorage\Http;

/**
 * Standard request for the remote storage.
 */
class StorageRequest {
    /**
     * HTTP method for the storage request.
     *
     * @var string
     */
    var $method;

    /**
     * Name of the store.
     *
     * @var string|null
     */
    var $store_name;

    /**
     * Key that the request refers to.
     *
     * @var string|null
     */
    var $key;

    /**
     * Flag indicates key-listing mode.
     *
     * @var bool
     */
    var $keys_listing_mode;

    /**
     * HTTP request body, if any.
     *
     * @var string
     */
    var $body;

    /**
     * Context array to store more background information
     * of the request. Can be useful for customize
     * StorageInterface creation process.
     *
     * @var array
     */
    var $context;

    /**
     * Class constructor.
     *
     * @param string      $method             HTTP method used for the request.
     * @param string|null $store_name         The store name specified.
     * @param string|null $key                The key to retrieve, if any.
     * @param boolean     $keys_listing_mode  If the request is for listing all the keys.
     * @param string      $body               The HTTP reqeust body, if any.
     * @param array       $context            Optional array to store extra information.
     */
    public function __construct(
        string $method,
        ?string $store_name,
        ?string $key,
        bool $keys_listing_mode,
        string $body,
        array $context = []
    )
    {
        $this->method = strtoupper($method);
        $this->store_name = $store_name ?: null;
        $this->key = $key ?: null;
        $this->keys_listing_mode = $keys_listing_mode;
        $this->body = $body;
        $this->context = $context;
    }
}