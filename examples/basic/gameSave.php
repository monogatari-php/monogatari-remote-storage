<?php

/**
 * Basic example of how the Monogatari RemoteStroage library works.
 *
 * Requires the "guzzlehttp/psr7" library. Run `composer install` to
 * install it through dev-dependencies before trying the example.
 *
 * Requires a copy of the Monogatari distribution source code to work.
 * Please run `composer test:download:monogatari` to download, then
 * serve this folder. Everything should work.
 */

use GuzzleHttp\Psr7\HttpFactory;
use Monogatari\RemoteStorage\Http\Controller;
use Monogatari\RemoteStorage\StorageEngine\FileSystemStorage;
use Monogatari\RemoteStorage\StorageEngine\SimpleStorageFactory;

require_once __DIR__ . '/../../vendor/autoload.php';

$httpFactory = new HttpFactory();
$controller = new Controller(
    new SimpleStorageFactory(fn () => (new FileSystemStorage('./gameSave.json'))),
    $httpFactory, // StreamFactoryInterface implementation
    $httpFactory, // ResponseFactoryInterface implementation
);
Controller::emit($controller->handleRequest(Controller::requestFromEnvironment()));
