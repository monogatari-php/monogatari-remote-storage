<?php
namespace Monogatari\RemoteStorage\Test\Monogatari\RemoteStorage\Exception;

use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;

/**
 * @covers \Monogatari\RemoteStorage\Exception\StorageKeyNotFound
 */
class StorageKeyNotFoundTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testConstructor()
    {
        $key = 'key-' . rand(1, 100);
        $e = new StorageKeyNotFound($key);

        $this->assertEquals($key, $e->key());
        $this->assertStringContainsString('"' . $key . '"', $e->getMessage());
    }
}