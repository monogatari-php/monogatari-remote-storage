<?php
namespace Monogatari\RemoteStorage\Test\Monogatari\RemoteStorage\Http;

use Monogatari\RemoteStorage\Http\StorageRequest;

/**
 * @covers Monogatari\RemoteStorage\Http\StorageRequest
 */
class StorageRequestTest extends \Codeception\Test\Unit
{

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testSomeFeature()
    {
        $r = new StorageRequest(
            'hello method',
            'hello store name',
            'hello key',
            true,
            'hello body'
        );
        $this->assertEquals('HELLO METHOD', $r->method);
        $this->assertEquals('hello store name', $r->store_name);
        $this->assertEquals('hello key', $r->key);
        $this->assertEquals(true, $r->keys_listing_mode);
        $this->assertEquals('hello body', $r->body);
        $this->assertEquals([], $r->context);

        $r = new StorageRequest(
            'hello method',
            'hello store name',
            'hello key',
            true,
            'hello body',
            ['hello' => 'world']
        );
        $this->assertEquals('HELLO METHOD', $r->method);
        $this->assertEquals('hello store name', $r->store_name);
        $this->assertEquals('hello key', $r->key);
        $this->assertEquals(true, $r->keys_listing_mode);
        $this->assertEquals('hello body', $r->body);
        $this->assertEquals(['hello' => 'world'], $r->context);
    }
}