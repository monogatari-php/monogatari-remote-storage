<?php
namespace Monogatari\RemoteStorage\Test\StorageEngine;

use Monogatari\RemoteStorage\Exception\StorageKeyNotFound;
use Monogatari\RemoteStorage\Exception\StorageUnreadable;
use Monogatari\RemoteStorage\Exception\StorageUnwritable;
use Monogatari\RemoteStorage\Http\StorageRequest;
use Monogatari\RemoteStorage\StorageEngine\FileSystemStorage;

/**
 * @covers \Monogatari\RemoteStorage\StorageEngine\FileSystemStorage
 */
class FileSystemStorageTest extends \Codeception\Test\Unit
{

    private function getPath()
    {
        return './FileSystemStorageTest.json';
    }

    protected function _before()
    {
    }

    protected function _after()
    {
        if (is_file($this->getPath())) {
            unlink($this->getPath());
        }
    }

    /**
     * Test the getAll method.
     */
    public function testGetAll()
    {
        $all = [
            'hello' => 'world',
            'foo' => ['bar1', 'bar2'],
        ];
        file_put_contents($this->getPath(), json_encode($all));

        $storage = new FileSystemStorage($this->getPath());
        $result = $storage->getAll();

        // Check the keys and values within.
        $this->assertEquals(array_keys($all), array_keys(get_object_vars($result)));
        $this->assertEquals($all['hello'], $result->hello);
        $this->assertEquals($all['foo'], $result->foo);
    }

    /**
     * Test the get method.
     */
    public function testGet()
    {
        $all = [
            'hello' => 'world',
            'foo' => ['bar1', 'bar2'],
        ];
        file_put_contents($this->getPath(), json_encode($all));

        $storage = new FileSystemStorage($this->getPath());
        $result = $storage->get('hello');

        // Check all values of all keys
        $this->assertEquals($all['hello'], $storage->get('hello'));
        $this->assertEquals($all['foo'], $storage->get('foo'));
    }

    /**
     * Test the get method.
     */
    public function testGetNonExistsKey()
    {
        $all = [
            'hello' => 'world',
            'foo' => ['bar1', 'bar2'],
        ];
        file_put_contents($this->getPath(), json_encode($all));
        $storage = new FileSystemStorage($this->getPath());

        $exception_thrown = false;
        try {
            $storage->get('non-exists key');
        } catch (StorageKeyNotFound $e) {
            $exception_thrown = true;
        }

        // Check all values of all keys
        $this->assertTrue($exception_thrown);
        $this->assertEquals('non-exists key', $e->key());
    }

    /**
     * Test the set method.
     */
    public function testSet()
    {
        $before = [
            'hello' => 'world',
            'foo' => ['bar1', 'bar2'],
        ];
        file_put_contents($this->getPath(), json_encode($before));

        $storage = new FileSystemStorage($this->getPath());
        $storage->set('mykey', 'myvar');

        // Check all values of all keys
        $after = json_decode(file_get_contents($this->getPath()), true, 512, JSON_THROW_ON_ERROR);

        $this->assertEquals($before['hello'], $after['hello']);
        $this->assertEquals($before['foo'], $after['foo']);
        $this->assertEquals('myvar', $after['mykey']);
    }

    /**
     * Test the remove method.
     */
    public function testRemove()
    {
        $before = [
            'hello' => 'world',
            'foo' => ['bar1', 'bar2'],
        ];
        file_put_contents($this->getPath(), json_encode($before));

        $storage = new FileSystemStorage($this->getPath());
        $storage->remove('hello');

        // Check all values of all keys
        $after = json_decode(file_get_contents($this->getPath()), true, 512, JSON_THROW_ON_ERROR);

        // Check if key is correctly removed and retain.
        $this->assertArrayNotHasKey('hello', $after);
        $this->assertArrayHasKey('foo', $after);
    }

    /**
     * Test the clear method.
     */
    public function testClear()
    {
        $before = [
            'hello' => 'world',
            'foo' => ['bar1', 'bar2'],
        ];
        file_put_contents($this->getPath(), json_encode($before));

        $storage = new FileSystemStorage($this->getPath());
        $storage->clear();
        $this->assertFileDoesNotExist($this->getPath());
    }

    public function testSaveToNewDir()
    {
        $dirroot = './FileSystemStorageTest';
        do {
            // try until the directory name is not an existing one.
            $dirname = $dirroot . '/Temp-' . rand(1,1000);
        } while (is_dir($dirname));

        $filename = 'tempfile-' . rand(1,100) . '.json';
        $fullpath = "{$dirname}/{$filename}";
        $storage = new FileSystemStorage($fullpath);
        $storage->set('hello', 'world');

        $this->assertDirectoryExists($dirname);
        $this->assertFileExists($fullpath);

        if (is_file($fullpath)) {
            unlink($fullpath);
        }
        if (is_dir($dirname)) {
            rmdir($dirname);
            rmdir($dirroot);
        }
    }

    public function testSaveToForbiddenDir()
    {
        do {
            // try until the directory name is not an existing one.
            $dirname = './FileSystemStorageTestTemp-' . rand(1,1000);
        } while (is_dir($dirname));
        mkdir($dirname, 0555);
        $this->assertDirectoryIsNotWritable($dirname);

        $filename = 'tempfile-' . rand(1,100) . '.json';
        $fullpath = "{$dirname}/{$filename}";
        $storage = new FileSystemStorage($fullpath);

        $exception_thrown = false;
        try {
            $storage->set('hello', 'world');
        } catch (StorageUnwritable $e) {
            $exception_thrown = true;
        }

        $this->assertTrue($exception_thrown);
        $this->assertStringContainsString(
            'unable to write to the parent directory',
            $e->getMessage()
        );
        $this->assertStringContainsString(
            $dirname,
            $e->getMessage()
        );

        if (is_file($fullpath)) {
            unlink($fullpath);
        }
        if (is_dir($dirname)) {
            rmdir($dirname);
        }
    }

    public function testSaveToReadonlyFile()
    {
        do {
            // try until the directory name is not an existing one.
            $filename = './FileSystemStorageTestTemp-' . rand(1,1000) . '.json';
        } while (file_exists($filename));

        file_put_contents($filename, '{}');
        chmod($filename, 0444); // set file to readonly
        $this->assertFileIsNotWritable($filename);

        $storage = new FileSystemStorage($filename);

        $exception_thrown = false;
        try {
            $storage->set('hello', 'world');
        } catch (StorageUnwritable $e) {
            $exception_thrown = true;
        }

        $this->assertTrue($exception_thrown);
        $this->assertStringContainsString(
            'unable to write to the file',
            $e->getMessage()
        );
        $this->assertStringContainsString(
            $filename,
            $e->getMessage()
        );

        if (is_file($filename)) {
            unlink($filename);
        }
    }
    public function testGetAllFromFileErrors()
    {

        file_put_contents($this->getPath(), '');
        $exception_thrown = false;
        try {
            new FileSystemStorage($this->getPath());
        } catch (StorageUnreadable $e) {
            $exception_thrown = true;
        }
        $this->assertTrue($exception_thrown);
        $this->assertEquals($this->getPath(), $e->getId());
        $this->assertEquals('json file is empty', $e->getReason());

        file_put_contents($this->getPath(), '{"incomplete json');
        $exception_thrown = false;
        try {
            new FileSystemStorage($this->getPath());
        } catch (StorageUnreadable $e) {
            $exception_thrown = true;
        }
        $this->assertTrue($exception_thrown);
        $this->assertEquals($this->getPath(), $e->getId());
        $this->assertStringStartsWith('json decode failed: ', $e->getReason());

        file_put_contents($this->getPath(), '"string value"');
        $exception_thrown = false;
        try {
            new FileSystemStorage($this->getPath());
        } catch (StorageUnreadable $e) {
            $exception_thrown = true;
        }
        $this->assertTrue($exception_thrown);
        $this->assertEquals($this->getPath(), $e->getId());
        $this->assertEquals('data stored is not an object', $e->getReason());
    }
}