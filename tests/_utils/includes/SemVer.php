<?php

/**
 * Sematic version parser.
 *
 * A class to parsing and comparing sematic version strings.
 */
class SemVer
{
    /**
     * Major version number
     *
     * @var int
     */
    protected $major;

    /**
     * Minor version number
     *
     * @var int
     */
    protected $minor;

    /**
     * Patch number, if exists.
     *
     * @var string|null
     */
    protected $patch;

    /**
     * Constructor.
     *
     * @param string $versionString  Version string of XX.XX or XX.XX.XXXXX format.
     *                               Support any string in the patch suffix (.XXXXX part in XX.XX.XXXXX).
     */
    function __construct(string $versionString){
        if (!preg_match('/^(|v)(?<major>\d+)\.(?<minor>\d+)(|\.(?<patch>.+))$/', $versionString, $matches)) {
            throw new \InvalidArgumentException('The version string provided (' . $versionString . ') is not in X.X.XXX or X.X format.');
        }
        $this->major = (int) $matches['major'];
        $this->minor = (int) $matches['minor'];
        $this->patch = $matches['patch'] ?? null;
    }

    function __toString()
    {
        return ($this->patch !== null)
            ? "{$this->major}.{$this->minor}.{$this->patch}"
            : "{$this->major}.{$this->minor}";
    }

    /**
     * Parse an array of sematic version string into a sorted array
     * of SemVer from smallest to largest.
     *
     * @param string[] $array  An array of sematic version string.
     *
     * @return SemVer[]  A sorted array.
     */
    public static function parseSorted(array $array): array
    {
        $versions = array_map(fn($version) => new SemVer((string) $version), $array);
        usort($versions, function (SemVer $v1, SemVer $v2) {
            if (($result = $v1->major() <=> $v2->major()) !== 0) return $result;
            if (($result = $v1->minor() <=> $v2->minor()) !== 0) return $result;
            return $v1->patch() <=> $v2->patch();
        });
        return $versions;
    }

    /**
     * Get the major version integer.
     *
     * @return integer
     */
    function major(): int
    {
        return $this->major;
    }

    /**
     * Get the minor version integer.
     *
     * @return integer
     */
    function minor(): int
    {
        return $this->minor;
    }

    /**
     * Get the patch identifier as string.
     *
     * @return string
     */
    function patch(): ?string
    {
        return $this->patch;
    }
}
